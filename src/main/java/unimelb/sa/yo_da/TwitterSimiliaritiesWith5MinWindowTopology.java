package unimelb.sa.yo_da;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import unimelb.sa.yo_da.bolt.*;
import unimelb.sa.yo_da.spout.TwitterSimiliaritiesSpout;

public class TwitterSimiliaritiesWith5MinWindowTopology {

  public static void main(String[] args) {
    TopologyBuilder builder = new TopologyBuilder();

    builder.setSpout("TwitterSimiliaritiesSpout", new TwitterSimiliaritiesSpout(), 10);
    builder.setBolt("FilterForSimiliaritiesBolt", new FilterForSimiliaritiesBolt(), 1).fieldsGrouping(
        "TwitterSimiliaritiesSpout", new Fields("user-screenname", "tweet-message"));
    builder.setBolt("TumblingWindowWithFrequentAlgBolt", new TumblingWindowWithFrequentAlgBolt(), 1).fieldsGrouping(
        "FilterForSimiliaritiesBolt", new Fields("user-screenname", "messages-tokened"));
    builder.setBolt("UserSimilarityBolt", new UserSimilarityBolt(), 1).fieldsGrouping(
        "TumblingWindowWithFrequentAlgBolt", new Fields("tumbling-window"));
    builder.setBolt("UserSimilarityPrintBoltFive", new UserSimilarityPrintBoltFive(),1).fieldsGrouping(
            "UserSimilarityBolt",new Fields("userPair","sim"));

    //Counting Tweets
    builder.setBolt("TwitterCountBolt", new TwitterCountBolt(), 1).fieldsGrouping(
        "TwitterSimiliaritiesSpout", new Fields("user-screenname", "tweet-message"));

    Config conf = new Config();
    conf.setDebug(false);
    conf.put("windowSize",5);

    LocalCluster cluster = new LocalCluster();
    cluster.submitTopology("twitter-user-similiarities", conf, builder.createTopology());
  }
}
