package unimelb.sa.yo_da;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import unimelb.sa.yo_da.bolt.FrequentBolt;
import unimelb.sa.yo_da.bolt.TwitterCountBolt;
import unimelb.sa.yo_da.spout.TwitterUserCountSpout;

public class TwitterUserCountTopology {

  public static void main(String[] args)
      throws AlreadyAliveException, InvalidTopologyException, InterruptedException {
    TopologyBuilder builder = new TopologyBuilder();

    builder.setSpout("TwitterUserCountSpout", new TwitterUserCountSpout(), 10);
    builder.setBolt("FrequentBolt", new FrequentBolt(), 1).fieldsGrouping(
        "TwitterUserCountSpout", new Fields("twitter-user-screenname"));
    //Counting Tweets
    builder.setBolt("TwitterCountBolt", new TwitterCountBolt(), 1).fieldsGrouping(
        "TwitterUserCountSpout", new Fields("twitter-user-screenname"));

    Config conf = new Config();
    conf.setDebug(false);
//    conf.put("K",1000);
    conf.put("K",10000);
//    conf.put("K",100000);
    conf.put("minsToWait",60);

    LocalCluster cluster = new LocalCluster();
    cluster.submitTopology("twitter-user-counting", conf, builder.createTopology());
  }
}
