package unimelb.sa.yo_da;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import unimelb.sa.yo_da.bolt.FrequentBolt;
import unimelb.sa.yo_da.bolt.RemoveNonEnglishWordBolt;
import unimelb.sa.yo_da.bolt.TwitterCountBolt;
import unimelb.sa.yo_da.bolt.WordBolt;
import unimelb.sa.yo_da.spout.TwitterWordCountSpout;

public class TwitterWordCountTopology {

  public static void main(String[] args)
      throws AlreadyAliveException, InvalidTopologyException, InterruptedException {
    TopologyBuilder builder = new TopologyBuilder();

    builder.setSpout("TwitterWordCountSpout", new TwitterWordCountSpout(), 10);
    builder.setBolt("filteringNonEnglishWords", new RemoveNonEnglishWordBolt(), 1).fieldsGrouping(
        "TwitterWordCountSpout", new Fields("tweet-message"));
    builder.setBolt("WordBolt", new WordBolt(), 1).fieldsGrouping("filteringNonEnglishWords",
                                                                  new Fields("tweet-message"));
    builder.setBolt("FrequentBolt", new FrequentBolt(), 1).fieldsGrouping(
        "WordBolt", new Fields("word"));
    //Counting Tweets
    builder.setBolt("TwitterCountBolt", new TwitterCountBolt(), 1).fieldsGrouping(
        "TwitterWordCountSpout", new Fields("tweet-message"));

    Config conf = new Config();
    conf.setDebug(false);
    conf.put("K",500);
    conf.put("minsToWait",60);

    LocalCluster cluster = new LocalCluster();
    cluster.submitTopology("twitter-frequent-words-counting", conf, builder.createTopology());
  }
}
