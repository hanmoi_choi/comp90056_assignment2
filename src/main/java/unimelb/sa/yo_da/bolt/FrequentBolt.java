package unimelb.sa.yo_da.bolt;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;
import backtype.storm.utils.Time;
import unimelb.sa.yo_da.util.ValueComparator;

public class FrequentBolt extends BaseRichBolt {

  private Long K;
  private Integer id;
  private String name;
  private ConcurrentHashMap<String, Integer> counters;
  private ValueComparator bvc;
  private TreeMap<String,Integer> sortedMap;
  private Long minsToWait;
  private int startTime;
  @Override
  public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
    K = (Long) map.get("K");
    minsToWait = (Long) map.get("minsToWait");
    this.counters = new ConcurrentHashMap<String, Integer>();
    this.name = topologyContext.getThisComponentId();
    this.id = topologyContext.getThisTaskId();
    startTime = Time.currentTimeSecs();
  }
  private void printTop20Words(){
    System.out.println("-- Word Counter [" + name + "-" + id + "] --");
    this.bvc = new ValueComparator(counters);
    this.sortedMap = new TreeMap<String,Integer>(bvc);
    sortedMap.putAll(counters);
    int i = 0;
    for (Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
      System.out.println(entry.getKey() + ": " + entry.getValue());
      i++;
      if(i == 20) break;
    }
  }

  @Override
  public void execute(Tuple tuple) {
    String str = tuple.getString(0);
    if (counters.containsKey(str)) {
      Integer c = counters.get(str) + 1;
      counters.put(str, c);
    } else if(counters.size() < K-1) {
      counters.put(str, 1);
    } else{
      for (Map.Entry<String, Integer> entry : counters.entrySet()) {
        String key = entry.getKey();
        Integer value = entry.getValue();
        value -= 1;
        if(value == 0){
          counters.remove(key);
        } else {
          counters.put(key, value);
        }
      }
    }
    if(Time.currentTimeSecs() - startTime > 60 * minsToWait){
      startTime = Time.currentTimeSecs();
      printTop20Words();
    }
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
//    outputFieldsDeclarer.declare(new Fields("word", "frequency"));
  }
}
