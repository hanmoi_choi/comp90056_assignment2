package unimelb.sa.yo_da.bolt;

import com.google.common.base.CharMatcher;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class RemoveNonEnglishWordBolt extends BaseRichBolt {

  private static final long serialVersionUID = 1L;
  private OutputCollector collector;

  @Override
  public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
    this.collector = collector;

  }

  @Override
  public void execute(Tuple tuple) {
    String message = tuple.getString(0);
    String newMessage = CharMatcher.ASCII.retainFrom(message);
    String regex = "\\(?\\b(http://|www[.])[-A-Za-z0-9+&/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]";
    newMessage = newMessage.replaceAll(regex, "");
    newMessage = newMessage.replaceAll("[+&/%?=~_()|!:,.;]","");

    collector.emit(new Values(newMessage));
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    outputFieldsDeclarer.declare(new Fields("tweet-message"));
  }
}
