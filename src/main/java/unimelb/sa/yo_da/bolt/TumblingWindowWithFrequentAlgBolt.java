package unimelb.sa.yo_da.bolt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Time;
import unimelb.sa.yo_da.util.ValueComparator;

public class TumblingWindowWithFrequentAlgBolt  extends BaseRichBolt {

  private OutputCollector collector;
  private Integer id;
  private String name;
  private ConcurrentHashMap<String, List<String>> tumblingWindow;
  private int startTime;
  private Long windowSize;

  @Override
  public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
    this.tumblingWindow = new ConcurrentHashMap<String, List<String>>();
    this.name = topologyContext.getThisComponentId();
    this.id = topologyContext.getThisTaskId();
    collector = outputCollector;
    windowSize = (Long) map.get("windowSize");
    startTime = Time.currentTimeSecs();
  }

  @Override
  public void execute(Tuple tuple) {
    String userScreenName = tuple.getString(0);
    HashSet<String> message = (HashSet<String>) tuple.getValue(1);

    if (tumblingWindow.containsKey(userScreenName)) {
      Iterator<String> iterator = message.iterator();
      while (iterator.hasNext()){
        String msg = iterator.next();
        tumblingWindow.get(userScreenName).add(msg);
      }
    } else{
      tumblingWindow.put(userScreenName, new ArrayList<String>());
      Iterator<String> iterator = message.iterator();
      while (iterator.hasNext()){
        String msg = iterator.next();
        tumblingWindow.get(userScreenName).add(msg);
      }
    }
    if(Time.currentTimeSecs() - startTime > 60 * windowSize){
      startTime = Time.currentTimeSecs();
      Map<String, List<String>> windowToPass = new HashMap<String, List<String>>();
      windowToPass.putAll(tumblingWindow);
      collector.emit(new Values(windowToPass));
      tumblingWindow.clear();
    }
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    outputFieldsDeclarer.declare(new Fields("tumbling-window"));
  }
}