package unimelb.sa.yo_da.bolt;

import com.google.common.base.CharMatcher;

import java.util.HashSet;
import java.util.Map;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Time;
import unimelb.sa.yo_da.tools.StopWorldRemoval;

public class TwitterCountBolt extends BaseRichBolt {
  private OutputCollector oc;
  private long coutingOfTweets;
  private int startTime;
  private int minsElapsed;
  @Override
  public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
    oc = outputCollector;
    coutingOfTweets = 0;
    minsElapsed = 0;
    startTime = Time.currentTimeSecs();
  }

  @Override
  public void execute(Tuple tuple) {
    coutingOfTweets += 1;

    if(Time.currentTimeSecs() - startTime > 60 * 1){
      minsElapsed += 1;
      startTime = Time.currentTimeSecs();
      System.out.println("Total Tweets: " + coutingOfTweets);
      System.out.println("Tweets Per Min: " + (coutingOfTweets/(60*minsElapsed)));
    }
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
  }
}
