package unimelb.sa.yo_da.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import java.util.*;

/**
 * A bolt to calculate user similarities from a given
 * window of data
 */
public class UserSimilarityBolt extends BaseRichBolt
{

    OutputCollector oc;

    @Override
    public void prepare(Map map, TopologyContext topologyContext,
                        OutputCollector outputCollector) {
        // To change body of implemented methods use File | Settings |
        // File Templates.
        oc = outputCollector;
    }

    @Override
    public void execute(Tuple tuple) {
        /***********************************/
        HashMap<String, ArrayList<String>> allVals =
                (HashMap<String, ArrayList<String>>) tuple.getValue(0);

        // then get the window of data from the incoming tuple
        /**********************************/

        ArrayList<String> users = new ArrayList<String>(allVals.keySet());
        Collections.sort(users);
        // The abovesorting allows efficient calculation of user
        // combinations.

        Iterator<String> iter = users.iterator();
        while (iter.hasNext()) {
            String user1 = iter.next();

            for (String user2 : users) {

                if (!user1.equals(user2)) {

                    /** get user 1's words, and unique words */
                    ArrayList<String> user1words = allVals.get(user1);
                    HashSet<String> user1uniquewords =
                            new HashSet<String>();
                    user1uniquewords.addAll(user1words);

                    /** get user 2's words, and unique words */
                    ArrayList<String> user2words = allVals.get(user2);
                    HashSet<String> user2uniquewords =
                            new HashSet<String>();
                    user2uniquewords.addAll(user2words);

                    // Variables for calculation
                    int fwi = 0;
                    int fwj = 0;
                    double sqWFreqI = 0;
                    double sqWFreqJ = 0;
                    int numeratorSum = 0;
                    double denominator = 0;
                    double finalAns = 0;

                    // calculating g(wi,wj) for all wi, wj
                    for (String wi : user1uniquewords) {
                        fwi = Collections.frequency(user1words, wi);

                        for (String wj : user2uniquewords) {
                            fwj = Collections.frequency(user2words, wj);
                            if (wi.equals(wj)) {
                                numeratorSum += (fwi * fwj);
                            }

                        }
                    }

                    // We need to calculate the lengths separately, as
                    // one document may be longer than the other.

                    // calculate sum of squared frequencies
                    // (fwi)^2 for all wi
                    for (String wi : user1uniquewords) {
                        fwi = Collections.frequency(user1words, wi);
                        sqWFreqI += (double) (fwi * fwi);
                    }

                    // calculate sum of squared frequencies
                    // (fwj)^2 for all wj
                    for (String wj : user2uniquewords) {
                        fwj = Collections.frequency(user2words, wj);
                        sqWFreqJ += (double) (fwj * fwj);
                    }

                    // finally, calculate the denominator expression
                    denominator =
                            Math.sqrt(sqWFreqI) * Math.sqrt(sqWFreqJ);

                    // perform the final calculation of similarity
                    // and submit to the outgoing stream
                    if (denominator == 0) {
                        finalAns = 0.0; // to avoid NaN similarity
                    } else {
                        finalAns = numeratorSum / denominator;
                    }

                    String tup = new String(user1 + ", " + user2);
                    oc.emit(new Values(tup, finalAns));

                    /*
                     * // testing statements String test1 = new
                     * String("user1"); String test2 = new
                     * String("user2"); double sim1 = 0.0; double sim2 =
                     * 1.0; oc.emit(new Values("user1, user2",sim1));
                     * oc.emit(new Values("user1, user2",sim2));
                     */
                    oc.ack(tuple);

                }
            }
            iter.remove();

        }
    }

    @Override
    public void declareOutputFields(
            OutputFieldsDeclarer outputFieldsDeclarer) {
        // To change body of implemented methods use File | Settings |
        // File Templates.
        outputFieldsDeclarer.declare(new Fields("userPair", "sim"));
    }
}
