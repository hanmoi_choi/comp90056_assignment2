package unimelb.sa.yo_da.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: streamsadmin
 * Date: 9/20/13
 * Time: 7:42 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * A Bolt to output user similarities. If users
 * are similar i.e. similarity >= 0.65, then they are written to the file
 * "similarities_twenty.txt"
 */
public class UserSimilarityPrintBoltTwenty extends BaseRichBolt
{
    OutputCollector oc;

    @Override
    public void prepare(Map map, TopologyContext topologyContext,
                        OutputCollector outputCollector) {
        oc = outputCollector;

    }

    @Override
    public void execute(Tuple tuple) {

        /** Receive incoming tuples */
        String up = (String) tuple.getValue(0);
        Double sim = (Double) tuple.getDouble(1);
        // System.out.println("map is: " + simScoreMap);
        // System.out.println("recd userpair: " + up + " sim is: " +
        // sim);

        try {
            File simFile = new File("similarities_twenty.txt");
            if (!simFile.exists()) {
                simFile.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(simFile, true);
            OutputStreamWriter osw = new OutputStreamWriter(fos);
            Writer w = new BufferedWriter(osw);
            Date date = new Date();
            String dateformat =
                    new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(date);

            if (sim >= 0.65) {


                String finalString =
                        dateformat + ", " + up + ", " + sim;
                w.write(finalString + "\n");
            }

            w.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            System.err.println("ERROR CREATING OR WRITING TO OUTPUT FILE");
            e.printStackTrace();
        }

    }

    @Override
    public void declareOutputFields(
            OutputFieldsDeclarer outputFieldsDeclarer) {
        // To change body of implemented methods use File | Settings |
        // File Templates.
    }

}
