package unimelb.sa.yo_da.bolt;

import com.google.common.base.CharMatcher;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import unimelb.sa.yo_da.tools.StopWorldRemoval;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;

public class WordBolt extends BaseRichBolt {

  HashSet<String> stopSet;
  OutputCollector oc;


  @Override
  public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
    //To change body of implemented methods use File | Settings | File Templates.
    oc = outputCollector;
    stopSet = StopWorldRemoval.createStopWordSet();
  }

  @Override
  public void execute(Tuple tuple) {
    String sentence = tuple.getString(0);
    String[] word = sentence.toLowerCase().split(" ");
    for (String w : word) {
      if (!stopSet.contains(w)) {
        w = CharMatcher.WHITESPACE.and(CharMatcher.BREAKING_WHITESPACE).trimFrom(w);
        if (w.length() >= 2 && w.matches("\\d+")== false && w.matches("@.+") == false) {
          oc.emit(new Values(w));
        }
      }
    }
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    outputFieldsDeclarer.declare(new Fields("word"));
  }
}
