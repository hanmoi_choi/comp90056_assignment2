package unimelb.sa.yo_da.spout;

import twitter4j.FilterQuery;
import twitter4j.Status;
import twitter4j.TwitterStreamFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

public class TwitterSimiliaritiesSpout extends TwitterSpout {

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    outputFieldsDeclarer.declare(new Fields("user-screenname", "tweet-message"));
  }

  protected void setTwitterConnectionAndGetClient(){
    Configuration configuration = new ConfigurationBuilder()
        .setJSONStoreEnabled(true)
        .build();
    twitterStream = new TwitterStreamFactory(configuration).getInstance();
    twitterStream.setOAuthConsumer(consumerKey, consumerSecret);
    twitterStream.setOAuthAccessToken(new AccessToken(token, secret));
    twitterStream.addListener(statusListener);

    FilterQuery query= new FilterQuery();
    double[][] loc={{-122.75,36.8}, {-121.75,37.8}};   //no filtering in results shown
    // from website San Francisco:  -122.75,36.8,-121.75,37.8   (http://  apiwiki.twitter.com/Streaming-API-Documentation#locations)
    //double[][] loc={{-122.75,36.8},  {-121.75,37.8}};                           //tried this too, but
    query.locations(loc);
    twitterStream.filter(query);
    twitterStream.sample();

  }

  @Override
  public void nextTuple() {
    Status ret = blockingQueue.poll();
    if(ret==null) {
      Utils.sleep(5);
    } else if(ret.getUser().getLang().equals("en")){
      outputCollector.emit(new Values(ret.getUser().getScreenName(), ret.getText()));
    }
  }

}
