package unimelb.sa.yo_da.spout;

import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;

import backtype.storm.Config;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

public abstract class TwitterSpout extends BaseRichSpout {
  protected SpoutOutputCollector outputCollector;
  protected String consumerKey;
  protected String consumerSecret;
  protected String token;
  protected String secret;
  protected LinkedBlockingQueue<Status> blockingQueue;
  protected TwitterStream twitterStream;
  protected StatusListener statusListener;


  @Override
  public void close() {
    super.close();
    twitterStream.shutdown();
  }

  @Override
  public Map<String, Object> getComponentConfiguration() {
    Config ret = new Config();
    ret.setMaxTaskParallelism(1);
    return ret;
  }



  @Override
  public void open(Map map, TopologyContext topologyContext,
                   SpoutOutputCollector spoutOutputCollector) {
    outputCollector = spoutOutputCollector;
    blockingQueue = new LinkedBlockingQueue<Status>(100);
    createStatusListener();
    readProperties();
    setTwitterConnectionAndGetClient();
  }

  protected abstract void setTwitterConnectionAndGetClient();

  protected void createStatusListener() {
    statusListener = new StatusListener() {
      @Override
      public void onStatus(Status status) {
        blockingQueue.offer(status);
      }

      @Override
      public void onDeletionNotice(StatusDeletionNotice sdn) {
      }

      @Override
      public void onTrackLimitationNotice(int i) {
      }

      @Override
      public void onScrubGeo(long l, long l1) {
      }

      @Override
      public void onStallWarning(StallWarning stallWarning) {

      }

      @Override
      public void onException(Exception e) {
      }

    };
  }

  private void readProperties(){
    Properties prop = new Properties();
    try {
      //load a properties file
      String absolutePath = null;
      URL resource = getClass().getClassLoader().getResource("twitter.properties");
      absolutePath = URLDecoder.decode(resource.getFile(), "utf-8");
      prop.load(new FileInputStream(absolutePath));

      //get the property value and print it out
      consumerKey = prop.getProperty("consumer.key");
      consumerSecret = prop.getProperty("consumer.secret");
      token = prop.getProperty("access.token");
      secret = prop.getProperty("access.secret");

    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }
}
