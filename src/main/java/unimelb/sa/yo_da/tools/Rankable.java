package unimelb.sa.yo_da.tools;

public interface Rankable extends Comparable<Rankable> {

  Object getObject();

  long getCount();

}
